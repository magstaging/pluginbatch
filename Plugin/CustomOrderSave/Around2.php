<?php

namespace Mbs\PluginBatch\Plugin\CustomOrderSave;

use Magento\Framework\App\RequestInterface;

class Around2
{
    /**
     * @var \Mbs\PluginBatch\Logger
     */
    private $logger;

    public function __construct(
        \Mbs\PluginBatch\Logger $logger
    ) {
        $this->logger = $logger;
    }

    public function aroundDispatch(\Mbs\PluginBatch\Controller\Order\Save $subject, $proceed, RequestInterface $request)
    {
        $this->logger->addLog('before call around Magento dispatch priority 2');

        $result = $proceed($request);

        $this->logger->addLog('after call around Magento priority 2');

        return $result;
    }
}
