<?php

namespace Mbs\PluginBatch\Plugin\CustomOrderSave;

use Magento\Framework\App\RequestInterface;
use Magento\Framework\Controller\ResultFactory;

class Around5
{
    /**
     * @var \Mbs\PluginBatch\Logger
     */
    private $logger;
    /**
     * @var ResultFactory
     */
    private $resultFactory;

    public function __construct(
        \Mbs\PluginBatch\Logger $logger,
        \Magento\Framework\Controller\ResultFactory $resultFactory
    ) {
        $this->logger = $logger;
        $this->resultFactory = $resultFactory;
    }

    public function aroundDispatch(\Mbs\PluginBatch\Controller\Order\Save $subject, $proceed, RequestInterface $request)
    {
        $this->logger->addLog('before call around Magento dispatch priority 5');

        //$result = $proceed($request);
        /** @var \Magento\Framework\View\Result\Page resultPage */
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $result = $resultPage;

        $this->logger->addLog('after call around Magento priority 5');

        return $result;
    }
}
