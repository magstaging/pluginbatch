<?php

namespace Mbs\PluginBatch\Plugin\CustomOrderSave;

use Magento\Framework\App\RequestInterface;

class Before4
{
    /**
     * @var \Mbs\PluginBatch\Logger
     */
    private $logger;

    public function __construct(
        \Mbs\PluginBatch\Logger $logger
    ) {
        $this->logger = $logger;
    }

    public function beforeDispatch(\Mbs\PluginBatch\Controller\Order\Save $subject, RequestInterface $request)
    {
        $this->logger->addLog('before call before Magento dispatch priority 4');

        return [$request];
    }
}
