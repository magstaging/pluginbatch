<?php

namespace Mbs\PluginBatch\Plugin\CustomOrderSave;

class After3
{
    /**
     * @var \Mbs\PluginBatch\Logger
     */
    private $logger;

    public function __construct(
        \Mbs\PluginBatch\Logger $logger
    ) {
        $this->logger = $logger;
    }

    public function afterDispatch(\Mbs\PluginBatch\Controller\Order\Save $subject, $result)
    {
        $this->logger->addLog('before call after Magento dispatch priority 3');

        return $result;
    }
}
