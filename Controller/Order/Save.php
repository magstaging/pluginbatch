<?php

namespace Mbs\PluginBatch\Controller\Order;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\ResponseInterface;

class Save extends Action
{
    private $logger;

    public function __construct(
        Context $context,
        \Mbs\PluginBatch\Logger $logger
    ) {
        parent::__construct($context);
        $this->logger = $logger;
    }

    public function dispatch(RequestInterface $request)
    {
        $this->logger->addLog('before disaptch');
        $result = parent::dispatch($request);
        $this->logger->addLog('after disaptch');

        return $result;
    }

    /**
     * Execute action based on request and return result
     *
     * Note: Request will be added as operation argument in future
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {

    }
}
