<?php

namespace Mbs\PluginBatch;

class Logger
{
    /**
     * @var \Zend\Log\Writer\StreamFactory
     */
    private $streamFactory;
    /**
     * @var \Zend\Log\LoggerFactory
     */
    private $loggerFactory;

    private static $defaultLogFile = 'debugplugin.log';

    public function __construct(
        \Zend\Log\Writer\StreamFactory $streamFactory,
        \Zend\Log\LoggerFactory $loggerFactory
    ) {
        $this->streamFactory = $streamFactory;
        $this->loggerFactory = $loggerFactory;
    }

    public function addLog($title, $message = '', $file = '')
    {
        /** @var \Zend\Log\Writer\Stream $stream */
        $stream = $this->streamFactory->create(['streamOrUrl' => $this->getLogFile($file)]);
        /** @var \Zend\Log\Logger $logger */
        $logger = $this->loggerFactory->create();
        $logger->addWriter($stream);
        $logger->info('message title: '.$title);
        if ($message) {
            if (is_array($message)) {
                $logger->info(print_r($message));
            } else {
                $logger->info('details: '.$message);
            }
        }
    }

    /**
     * @param $file
     * @return string
     */
    private function getLogFile($file)
    {
        if ($file == '') {
            $file = self::$defaultLogFile;
        }

        $logFileDir = BP.'/var/log/';

        if (!file_exists($logFileDir)) {
            mkdir($logFileDir, 0750, true);
        }

        return $logFileDir.$file;
    }

}
